# Easycomp

``` bash
npm install easycomp
```

### Use
##### All components:
``` bash
import Easycomp from 'easycomp'
import 'easycomp/dist/easycomp.css'

Vue.use(Easycomp)
```
##### Individual components:
``` bash
import { Select, Icon } from 'easycomp'
import 'easycomp/dist/easycomp.css'

Vue.use(Select)
Vue.use(Icon)
```
