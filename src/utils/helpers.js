export function inRange (x, min, max) {
  return ((x - min) * (x - max) <= 0)
}

export const isNull = val => val === null

export const isUndefined = val => val === undefined

export const isUndefinedOrNull = val => isUndefined(val) || isNull(val)

// Compute a class name with sizes
export const computeSizeClasses = (type, size, val) => {
  let className = type

  if (!size && (val === '' || val === true)) {
    return ''
  }

  if (isUndefinedOrNull(val) || val === false) {
    return ''
  }
  if (size) {
    className += `-${size}`
  }

  if (val === '' || val === true) {
    return className
  }
  className += `-${val}`
  return className
}
