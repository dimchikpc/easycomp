export const allowedJustify = [
  'start',
  'end',
  'center',
  'between',
  'around'
]

export const allowedSizes = [
  'sm',
  'md',
  'lg',
  'xl'
]
