import Icon from './Icon.vue'

export { Icon }
export default Vue => {
  Vue.component(Icon.name, Icon)
}
