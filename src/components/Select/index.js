import Select from './Select.vue'

export { Select }
export default Vue => {
  Vue.component(Select.name, Select)
}
