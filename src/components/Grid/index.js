import Container from './Container'
import Row from './Row'
import Col from './Col'

export { Container, Col, Row }

export default Vue => {
  Vue.component(Container.name, Container)
  Vue.component(Row.name, Row)
  Vue.component(Col.name, Col)
}
