import './styles/main.scss'
import * as components from './components'

const Easycomp = {
  install (Vue, options) {
    Object.values(components).forEach((component) => {
      Vue.component(component.name, component)
    })
  }
}

export default Easycomp

export { default as Icon } from './components/Icon'
export { default as Select } from './components/Select'
